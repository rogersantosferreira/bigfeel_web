<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('upload', 'APIController@upload');
Route::post('afinn/{id?}', 'APIController@afinn');
Route::post('emolex/{id?}', 'APIController@emolex');
Route::post('emoticons/{id?}', 'APIController@emoticons');
Route::post('emoticon_ds/{id?}', 'APIController@emoticon_ds');
Route::post('happiness_index/{id?}', 'APIController@happiness_index');
Route::post('mpqa/{id?}', 'APIController@mpqa');
Route::post('nrc/{id?}', 'APIController@nrc');
Route::post('opinion/{id?}', 'APIController@opinion');
Route::post('panas_t/{id?}', 'APIController@panas_t');
Route::post('sann/{id?}', 'APIController@sann');
Route::post('sasa/{id?}', 'APIController@sasa');
Route::post('sentic_net/{id?}', 'APIController@sentic_net');
Route::post('sentiment140/{id?}', 'APIController@sentiment140');
Route::post('senti_strength/{id?}', 'APIController@senti_strength');
Route::post('socal/{id?}', 'APIController@socal');
Route::post('stanford/{id?}', 'APIController@stanford');
Route::post('umigon/{id?}', 'APIController@umigon');
Route::post('vader/{id?}', 'APIController@vader');
Route::post('db_stanford/{id?}', 'APIController@db_stanford');
Route::post('rf_spark/{id?}', 'APIController@rf_spark');
Route::post('nb_spark/{id?}', 'APIController@nb_spark');
Route::post('cleanxml/{id?}', 'APIController@cleanxml');
Route::post('ssplit/{id?}', 'APIController@ssplit');
Route::post('pos/{id?}', 'APIController@pos');
Route::post('lemma/{id?}', 'APIController@lemma');
Route::post('ner/{id?}', 'APIController@ner');
Route::post('depparse/{id?}', 'APIController@depparse');
Route::post('coref/{id?}', 'APIController@coref');
Route::post('natlog/{id?}', 'APIController@natlog');
Route::post('openie/{id?}', 'APIController@openie');
ROute::post('composition/{id?}', 'APIController@composition');