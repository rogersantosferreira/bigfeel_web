<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::post('sentiment_analysis', 'SAController@analysis');
Route::post('upload', 'SAController@upload');
Route::get('download/{id}', 'SAController@download');
Route::get('download-example', function() {
	return response()->download(public_path("example-file.txt"));
});
/*Route::get('/testing', function(){
	return shell_exec('/cluster/spark/bin/spark-submit 2>&1');
});*/
