<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="img/apple-icon.png">
    <link rel="icon" type="image/png" href="img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>BigFeel</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link href="css/fonte.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css" />
    <!-- CSS Files -->
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/now-ui-kit.css?v=1.1.0" rel="stylesheet" />
    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link href="css/dropzone.css" rel="stylesheet" />
    <link href="js/plugins/jquery-multiselect/css/multi-select.css" rel="stylesheet" />
    <link href="css/demo.css" rel="stylesheet" />
</head>

<body class="landing-page sidebar-collapse">
    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg bg-primary fixed-top navbar-transparent " color-on-scroll="400">
        <div class="container">
            <div class="dropdown button-dropdown">
                <a href="#pablo" class="dropdown-toggle" id="navbarDropdown" data-toggle="dropdown">
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                    <span class="button-bar"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#o_que_e">What is BigFeel?</a>
                    <a class="dropdown-item" href="#justificativa">Goals and the project</a>
                </div>
            </div>
            <div class="navbar-translate">
                <a class="navbar-brand" href="http://www.ufla.br/portal/" rel="tooltip" title="Federal University of Lavras" data-placement="bottom" target="_blank">
                    BigFeel • UFLA
                </a>
                <button class="navbar-toggler navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-bar bar1"></span>
                    <span class="navbar-toggler-bar bar2"></span>
                    <span class="navbar-toggler-bar bar3"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse justify-content-end" id="navigation" data-nav-image="img/blurred-image-1.jpg">
                <ul class="navbar-nav">
                    <!--<li class="nav-item">
                        <a class="nav-link" href="../index.html">Back to Kit</a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="#testar_bigfeel">Testing BigFeel</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Questions?</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" rel="tooltip" title="Visit our GIT repository" data-placement="bottom" href="https://bitbucket.org/rogersantosferreira/bigfeel" target="_blank">
                            <i class="fa fa-bitbucket"></i>
                            <p class="d-lg-none d-xl-none">BitBucket</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- End Navbar -->
    <div class="wrapper">
        <div class="page-header page-header-small">
            <div class="page-header-image" data-parallax="true" style="background-image: url('img/bigdata2.jpg');">
            </div>
            <div class="container">
                <div class="content-center">
                    <h1 class="title">Welcome to BigFeel!</h1>
                    <!--<div class="text-center">
                        <a href="#pablo" class="btn btn-primary btn-icon btn-round">
                            <i class="fa fa-github"></i>
                        </a>
                    </div>-->
                </div>
            </div>
        </div>
        <div class="section section-about-us" id="o_que_e">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title">What is BigFeel?</h2>
                        <h5 class="description">
                            This BigFeel interface is a Web application that offers sentiment analysis of big data by a distributed processing environment. The tool available here is for open and free use, only for the experimental purpose.
                        </h5>
                    </div>
                </div>
                <div class="separator separator-primary"></div>
                <div class="section-story-overview" id="justificativa">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="image-container image-left" style="background-image: url('img/bigdata1.jpg')">
                                <!-- First image on the left side -->
                                <p class="blockquote blockquote-primary">"A dissertation project presented to the Collegiate of the Post-Graduation Program in Computer Science of the Federal University of Lavras as part of the requirements for obtaining a Master's degree in Computer Science."
                                    <!--<br>
                                    <br>-->
                                    <!--<small>-NOAA</small>-->
                                </p>
                            </div>
                            <!-- Second image on the left side of the article -->
                            <div class="image-container" style="background-image: url('img/bigdata3.jpg')"></div>
                        </div>
                        <div class="col-md-5">
                            <!-- First image on the right side, above the article -->
                            <div class="image-container image-right" style="background-image: url('img/capa.jpg')"></div>
                            <h3>Project justification, goals and proposal.</h3>
                            <p>Due to the speed, volume and variety with which the data is generated in digital format, the information retrieval of these, known as Big Data, has become one of the great challenges of the modern era. The sentiment analysis, also called opinion mining, seeks to identify the opinion, sentiment, evaluation, attitudes and / or emotions contained in unstructured texts, as is the case of publications in social networks.</p>
                            <p>Because it is a subject of great business interest, increasingly integrated with the Web, the sentiment analysis is one of the most active areas of research in large areas such as natural language processing, information retrieval and data mining. The present project seeks to present an approach for integrating methods of sentiment analysis in order to process large volumes of data in a distributed environment, using both the Spark platform and the Hadoop ecosystem, both of the Apache Foundation.</p>
                            <p>It is proposed to study the methods and techniques offered by Spark, as well as its ability to integrate with other applications that provide services for textual preprocessing, natural language processing and sentiment analysis. There will be an experimental evaluation of the effectiveness and efficiency of the various methods and tools.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-team text-center">
            <div class="container">
                <h2 class="title">Work Team</h2>
                <div class="team">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="team-player">
                                <img src="img/denilson.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                                <h4 class="title">Prof. Dr. Denilson Alves Pereira</h4>
                                <p class="category text-primary">Teacher advisor</p>
                                <p class="description">Adjunct Professor, Department of Computer Science (DCC), Federal University of Lavras (UFLA)</p>
                                <a href="http://lattes.cnpq.br/4120230814124499" class="btn btn-primary btn-icon btn-round" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                <a href="mailto:denilson.pereira@gmail.com" class="btn btn-primary btn-icon btn-round"><i class="fa fa-envelope"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="team-player">
                                <img src="img/roger.jpg" alt="Thumbnail Image" class="rounded-circle img-fluid img-raised">
                                <h4 class="title">Roger Santos Ferreira</h4>
                                <p class="category text-primary">Student</p>
                                <p class="description">Master degree student in Computer Science from the Federal University of Lavras (UFLA)</p>
                                <a href="http://lattes.cnpq.br/9961909524574149" class="btn btn-primary btn-icon btn-round" target="_blank"><i class="fa fa-file-text-o"></i></a>
                                <a href="mailto:rogersantosferreira@gmail.com" class="btn btn-primary btn-icon btn-round"><i class="fa fa-envelope"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section section-contact-us text-center" data-background-color="black" id="testar_bigfeel">
            <div class="container">
                <h2 class="title">Want to try BigFeel?</h2>
                <p class="description">Your feedback is very appreciated for us!</p>
                <div class="row">
                    <div class="col-lg-6 text-center col-md-8 ml-auto mr-auto">

                    <!-- Drozone form -->
                        <div class="col-md-12" style="padding: 0 40px;">
                            <form action="upload" class="dropzone" id="file-upload" data-placement="right" title="" data-content="Invalid input!">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                            </form>
                        </div>
                        
                        <br/>
                        
                        <!-- Sentiment Analysis form -->
                        <form action="sentiment_analysis" method="post" id="sentiment_analysis">
                            
                            <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                            <input type="hidden" name="file" id="file" required/>

                            <div class="input-group input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons users_circle-08"></i>
                                </span>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Your name..." data-placement="right" title="" data-content="Invalid input!" required>
                            </div>
                            <div class="input-group input-lg">
                                <span class="input-group-addon">
                                    <i class="now-ui-icons ui-1_email-85"></i>
                                </span>
                                <input type="email" class="form-control" name="email" id="email" placeholder="E-mail..." data-placement="right" title="" data-content="Invalid input!" required>
                            </div>
    						<div class="input-group input-lg">
                                <span class="input-group-addon">
                                    <i class="fa fa-cog"></i>
                                </span>
                                <select id="method" class="form-control method_selector" style="padding:0px; height: 47px;" name="method" data-placement="right" title="" data-content="Invalid input!" required>
    								<option value="" disabled selected>&nbsp;&nbsp;&nbsp;&nbsp;Choose a method...</option>
    								<option value="Afinn">&nbsp;&nbsp;&nbsp;&nbsp;Afinn</option>
    								<option value="Emolex">&nbsp;&nbsp;&nbsp;&nbsp;Emolex</option>
    								<option value="Emoticons">&nbsp;&nbsp;&nbsp;&nbsp;Emoticons</option>
    								<option value="EmoticonDS">&nbsp;&nbsp;&nbsp;&nbsp;EmoticonDS</option>
    								<option value="HappinessIndex">&nbsp;&nbsp;&nbsp;&nbsp;HappinessIndex</option>
    								<option value="MPQA">&nbsp;&nbsp;&nbsp;&nbsp;MPQA (Opinion Finder)</option>
    								<option value="NRCHashtag">&nbsp;&nbsp;&nbsp;&nbsp;NRCHashtag</option>
    								<option value="OpinionLexicon">&nbsp;&nbsp;&nbsp;&nbsp;OpinionLexicon</option>
    								<option value="PanasT">&nbsp;&nbsp;&nbsp;&nbsp;PANAS-t</option>
    								<option value="Sann">&nbsp;&nbsp;&nbsp;&nbsp;Sann</option>
    								<option value="Sasa">&nbsp;&nbsp;&nbsp;&nbsp;Sasa</option>
    								<option value="SenticNet">&nbsp;&nbsp;&nbsp;&nbsp;SenticNet</option>
    								<option value="Sentiment140">&nbsp;&nbsp;&nbsp;&nbsp;Sentiment140 Lexicon</option>
    								<option value="SentiStrength">&nbsp;&nbsp;&nbsp;&nbsp;SentiStrength</option>
    								<option value="SentiWordNet">&nbsp;&nbsp;&nbsp;&nbsp;SentiWordNet</option>
    								<option value="SoCal">&nbsp;&nbsp;&nbsp;&nbsp;SO-CAL</option>
    								<option value="Stanford">&nbsp;&nbsp;&nbsp;&nbsp;Stanford Recursive Deep Model</option>
    								<option value="Umigon">&nbsp;&nbsp;&nbsp;&nbsp;Umigon</option>
    								<option value="Vader">&nbsp;&nbsp;&nbsp;&nbsp;VADER</option>
    								<option value="db_stanford">&nbsp;&nbsp;&nbsp;&nbsp;Databricks Stanford</option>
    								<option value="spark_naive_bayes">&nbsp;&nbsp;&nbsp;&nbsp;Spark Naive Bayes [movies domain]</option>
    								<option value="spark_random_forest">&nbsp;&nbsp;&nbsp;&nbsp;Spark Random Forest [movies domain]</option>
                                    <option value="Composition">&nbsp;&nbsp;&nbsp;&nbsp;Composition of methods</option>
    							</select>
    							<!--<input type="text" class="form-control" placeholder="Escolha um método...">-->
                            </div>
                            
                            <!--<div class="textarea-container">
                                <textarea class="form-control" name="name" rows="4" cols="80" placeholder="Insira sua opinião / sugestão / crítica..."></textarea>
                            </div>-->
                            <div id="composition_methods_panel" align="left">
                                <div class="row">
                                    <div class="col-md-10 margin-left-40">
                                        <p>Select the methods and their weights in order to perform the composition analysis of them.<br/>
                                           PS: The sum of all weights necessarily needs to be 1.0.<br/>
                                           Example: Afinn,Emolex,EmoticonDS - 0.2,0.6,0.2
                                        </p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select multiple="multiple" id="composition_methods" name="composition_methods[]">
                                            <option value="Afinn">Afinn</option>
                                            <option value="Emolex">Emolex</option>
                                            <option value="Emoticons">Emoticons</option>
                                            <option value="EmoticonDS">EmoticonDS</option>
                                            <option value="HappinessIndex">HappinessIndex</option>
                                            <option value="MPQA">MPQA (Opinion Finder)</option>
                                            <option value="NRCHashtag">NRCHashtag</option>
                                            <option value="OpinionLexicon">OpinionLexicon</option>
                                            <option value="PanasT">PANAS-t</option>
                                            <option value="Sann">Sann</option>
                                            <option value="Sasa">Sasa</option>
                                            <option value="SenticNet">SenticNet</option>
                                            <option value="Sentiment140">Sentiment140 Lexicon</option>
                                            <option value="SentiStrength">SentiStrength</option>
                                            <option value="SentiWordNet">SentiWordNet</option>
                                            <option value="SoCal">SO-CAL</option>
                                            <option value="Stanford">Stanford Recursive Deep Model</option>
                                            <option value="Umigon">Umigon</option>
                                            <option value="Vader">VADER</option>
                                            <option value="db_stanford">Databricks Stanford</option>
                                            <option value="spark_naive_bayes">Spark Naive Bayes [movies domain]</option>
                                            <option value="spark_random_forest">Spark Random Forest [movies domain]</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            



                            <div class="send-button">
                                <input type="button" id="analysis-btn" class="btn btn-primary btn-round btn-block btn-lg" value="Analyse document">
                                <div class="text-center" id="loader">
                                    <i class="fa fa-cog fa-2x fa-spin"></i>&nbsp;Wait, processing...
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="row" id="results">
                    <div class="col-md-10 col-lg-8 col-xl-6 ml-auto mr-auto">
                        <br/>
                        <p class="category">Analysis result</p>
                        <!-- Nav tabs -->
                        <div class="card card-results">
                            <ul class="nav nav-tabs justify-content-center" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#result_console" role="tab" aria-expanded="true">
                                        <i class="fa fa-terminal"></i> Console
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#result_download" role="tab" aria-expanded="false">
                                        <i class="fa fa-download"></i> Output
                                    </a>
                                </li>
                            </ul>
                            <div class="card-body">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="result_console" role="tabpanel" aria-expanded="true">
                                    </div>
                                    <div class="tab-pane" id="result_download" role="tabpanel" aria-expanded="false">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="col-md-5" id="result_console" style="border:1px solid #CCC;">
                        console
                    </div>
                    <div class="col-md-2">&nbsp;</div>
                    <div class="col-md-5" id="result_download" style="border:1px solid #CCC;">
                        download
                    </div> -->
                </div>

            </div>
        </div>
		
		<div class="section section-contact-us text-center">
            <div class="container">
                <h2 class="title">About BigFeel methods</h2>
                <p class="description">Several methods have been integrated into the BigFeel environment. Here are his details:</p>
                <div class="row">
                    <div class="col-lg-12 text-center col-md-12 ml-auto mr-auto">
						<div class="card">
							<ul class="nav nav-tabs nav-tabs-neutral justify-content-center" role="tablist" data-background-color="orange">
								<li class="nav-item">
									<a class="nav-link active" data-toggle="tab" href="#afinn" role="tab">Afinn</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#emolex" role="tab">Emolex</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#emoticons" role="tab">Emoticons</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#emoticonds" role="tab">EmoticonDS</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#happinessindex" role="tab">HappinessIndex</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#mpq" role="tab">MPQA (Opinion Finder)</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#nrc" role="tab">NRC Hashtag</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#opinionlexicon" role="tab">Opinion Lexicon</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#panast" role="tab">PANAS-t</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#sann" role="tab">SANN</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#sasa" role="tab">SASA</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#senticnet" role="tab">SenticNet</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#sentiment140" role="tab">Sentiment140 Lexicon</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#sentistrength" role="tab">SentiStrength</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#sentiwordnet" role="tab">SentiWordNet</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#socal" role="tab">SO-CAL</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#stanford" role="tab">Stanford Recursive Deep Model</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#umigon" role="tab">Umigon</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#vader" role="tab">VADER</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#db_stanford" role="tab">Databricks Stanford</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#spark_nb" role="tab">Spark Naive Bayes</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" data-toggle="tab" href="#spark_rf" role="tab">Spark Random Forest</a>
								</li>
							</ul>
							<div class="card-body">
								<!-- Tab panes -->
								<div class="tab-content text-center">
									<div class="tab-pane active" id="afinn" role="tabpanel">
										<p>Builds a Twitter based sentiment Lexicon including Internet slangs and obscene words, with 2477 words. AFINN can be considered as an expansion of <a href="https://www.uvm.edu/pdodds/teaching/courses/2009-08UVM-300/docs/others/everything/bradley1999a.pdf">ANEW</a>, a dictionary created to provides emotional ratings for English words. ANEW dictionary rates words in terms of pleasure, arousal and dominance.</p>
									</div>
									<div class="tab-pane" id="emolex" role="tabpanel">
										<p>Builds a general sentiment Lexicon crowdsourcing supported. Each entry lists the association of a token with 8 basic sentiments: joy, sadness, anger, etc. defined by <a href="https://books.google.com.br/books?hl=pt-BR&lr=&id=k0mhAwAAQBAJ&oi=fnd&pg=PA197&dq=A+general+psychoevolutionary+theory+of+emotion&ots=kX-hNWhI_T&sig=AhfDZJP4tY5oya7pCZzcna6dg_o&redir_esc=y#v=onepage&q=A%20general%20psychoevolutionary%20theory%20of%20emotion&f=false">Plutchik R (1980)</a>. Proposed Lexicon includes unigrams and bigrams from Macquarie Thesaurus and also words from <a href="https://books.google.com.br/books?id=wdCad65xNvkC&hl=pt-BR&source=gbs_book_other_versions">General Inquirer</a> and <a href="http://nlp.cs.swarthmore.edu/~richardw/papers/miller1995-wordnet.pdf">WordNet</a>.</p>
									</div>
									<div class="tab-pane" id="emoticons" role="tabpanel">
										<p>Messages containing positive/negative emoticons are positive/negative. Messages without emoticons are not classified.</p>
									</div>
									<div class="tab-pane" id="emoticonds" role="tabpanel">
										<p>Creates a scored lexicon based on a large dataset of tweets. Its based on the frequency each lexicon occurs with positive or negative emotions.</p>
									</div>
									<div class="tab-pane" id="happinessindex" role="tabpanel">
	   									<p><a href="https://www.uvm.edu/pdodds/teaching/courses/2009-08UVM-300/docs/others/everything/bradley1999a.pdf">ANEW</a> dictionary based method. Using the ANEW valence values, the authors attempt to get a general valence value, which may be used as the degree of happiness of a sentence.</p>
									</div>
									<div class="tab-pane" id="mpq" role="tabpanel">
										<p>Performs subjectivity analysis trough a framework with lexical analysis former and a machine learning approach latter.</p>
									</div>
									<div class="tab-pane" id="nrchashtag" role="tabpanel">
										<p>Builds a lexicon dictionary using a Distant Supervised Approach. In a nutshell it uses known hashtags (i.e. #joy, #happy, etc.) to ‘classify’ the tweet. Afterwards, it verifies frequency each specific n-gram occurs in a emotion and calculates its Strong of Association with that emotion.</p>
									</div>
									<div class="tab-pane" id="opinionlexicon" role="tabpanel">
										<p>Focus on Product Reviews. Builds a Lexicon to predict polarity of product features phrases that are summarized to provide an overall score to that product feature.</p>
									</div>
									<div class="tab-pane" id="panast" role="tabpanel">
										<p>Detects mood fluctuations of users on Twitter. The method consists of an adapted version <a href="https://pdfs.semanticscholar.org/7124/10f2b20c831678f71db35ec01ba1b38bc842.pdf">(PANAS) Positive Affect Negative Affect Scale</a>, well-known method in psychology with a large set of words, each of them associated with one from eleven moods such as surprise, fear, guilt, etc.</p>
									</div>
									<div class="tab-pane" id="sann" role="tabpanel">
										<p>Proposed a Sentiment-Aware Nearest Neighbor (SANN) model, using an extension for the rule-based classifier of <a href="https://people.cs.pitt.edu/~wiebe/pubs/papers/emnlp05polarity.pdf">Wilson, Wiebe and Hoffmann (2005)</a>.
									</div>
									<div class="tab-pane" id="sasa" role="tabpanel">
										<p>Detects public sentiments on Twitter during the 2012 U.S. presidential election. It is based on the statistical model obtained from the classifier Naive Bayes on unigram features. It also explores emoticons and exclamations.</p>
									</div>
									<div class="tab-pane" id="senticnet" role="tabpanel">
										<p>Uses dimensionality reduction to infer the polarity of common sense concepts and hence provide a resource for mining opinions from text at a semantic, rather than just syntactic level.</p>
									</div>
									<div class="tab-pane" id="sentiment140" role="tabpanel">
										<p>A lexicon dictionary based on the same dataset used to train the Sentiment140 Method. The lexicon was built in a similar way to <a href="http://www.aclweb.org/anthology/S12-1033">NRC Hashtag of Mohammad (2012)</a> but authors used the occurrence of emoticons to classify the tweet as positive or negative. Then, the n-gram score was calculated based on the frequency of occurrence in each class of tweets.</p> 
									</div>
									<div class="tab-pane" id="sentistrength" role="tabpanel">
										<p>Builds a lexicon dictionary annotated by humans and improved with the use of Machine Learning.</p>
									</div>
									<div class="tab-pane" id="sentiwordnet" role="tabpanel">
										<p>Construction of a lexical resource for Opinion Mining based on <a href="http://nlp.cs.swarthmore.edu/~richardw/papers/miller1995-wordnet.pdf">WordNet</a>. The authors grouped adjectives, nouns, etc. in synonym sets (synsets) and associated three polarity scores (positive, negative and neutral) for each one.</p>
									</div>
									<div class="tab-pane" id="socal" role="tabpanel">
										<p>Creates a newLexicon with unigrams (verbs, adverbs, nouns and adjectives) and multi-grams (phrasal verbs and intensifiers) hand ranked with scale +5 (strongly positive) to –5 (strongly negative). Authors also included part of speech processing, negation and intensifiers.</p>
									</div>
									<div class="tab-pane" id="stanford" role="tabpanel">
										<p>Proposes a model called Recursive Neural Tensor Network (RNTN) that processes all sentences dealing with their structures and compute the interactions between them. This approach is interesting since RNTN take into account the order of words in a sentence, which is ignored in most of methods.</p>
									</div>
									<div class="tab-pane" id="umigon" role="tabpanel">
										<p>Disambiguates tweets using lexicon with heuristics to detect negations plus elongated words and hashtags evaluation.</p>
									</div>
									<div class="tab-pane" id="vader" role="tabpanel">
										<p>It is a human-validated sentiment analysis method developed for Twitter and social media contexts. VADER was created from a generalizable, valence-based, human-curated gold standard sentiment lexicon.</p>
									</div>
									<div class="tab-pane" id="db_stanford" role="tabpanel">
										<p>Databricks wrapper of Stanford CoreNLP for Apache Spark Dataframe API (2.0+).</p>
									</div>
									<div class="tab-pane" id="spark_nb" role="tabpanel">
										<p>Author's implementation of Naive Bayes Classifier using Apache Spark MLLib in Movies and TV domain.</p>
									</div>
									<div class="tab-pane" id="spark_rf" role="tabpanel">
										<p>Author's implementation of Random Forest Classifier using Apache Spark MLLib in Movies and TV domain.</p>
									</div>
								</div>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
		
        <footer class="footer footer-default">
            <div class="container">
                <nav>
                    <ul>
                        <li>
                            <a href="http://www.ufla.br/portal/">
                                UFLA
                            </a>
                        </li>
                        <li>
                            <a href="Sobre o projeto">
                                About
                            </a>
                        </li>
                        <li>
                            <a href="bitbuket">
                                Bitbucket
                            </a>
                        </li>
                        <li>
                            <a href="https://github.com/creativetimofficial/now-ui-kit/blob/master/LICENSE.md">
                                MIT License
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="copyright">
                    &copy;
                    <script>
                        document.write(new Date().getFullYear())
                    </script>, Created by Roger S. F.
                </div>
            </div>
        </footer>
    </div>
</body>
<!--   Core JS Files   -->
<script src="js/core/jquery.3.2.1.min.js" type="text/javascript"></script>
<script src="js/core/popper.min.js" type="text/javascript"></script>
<script src="js/core/bootstrap.min.js" type="text/javascript"></script>
<!--  Plugin for Switches, full documentation here: http://www.jque.re/plugins/version3/bootstrap.switch/ -->
<script src="js/plugins/bootstrap-switch.js"></script>
<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="js/plugins/nouislider.min.js" type="text/javascript"></script>
<!--  Plugin for the DatePicker, full documentation here: https://github.com/uxsolutions/bootstrap-datepicker -->
<script src="js/plugins/bootstrap-datepicker.js" type="text/javascript"></script>
<!-- Control Center for Now Ui Kit: parallax effects, scripts for the example pages etc -->
<script src="js/now-ui-kit.js?v=1.1.0" type="text/javascript"></script>
<script src="js/plugins/jquery.inputmask.bundle.min.js"></script>
<script src="js/plugins/jquery-multiselect/js/jquery.multi-select.js"></script>
<script src="js/dropzone.js"></script>

<script type="text/javascript">
    //function to upload file and deliver the uploaded name to the form that executes a sentiment analysis
    Dropzone.options.fileUpload = {
        paramName: "file",
        maxFilesize: 2, // MB
        maxFiles: 1,
        acceptedFiles: '.csv,.txt',
        addRemoveLinks: true,
        success: function(file, response){
            /*alert(response);*/
            var ele = document.getElementById('file');
            ele.value = response;
        }
    };


    $(function() {
        //$(":input").inputmask();

        $('.method_weight').inputmask('decimal', {
            radixPoint:".",
            autoGroup: true,
            digits: 1,
            digitsOptional: false,
            rightAlign: true
        });

        $('#loader').hide();
        $('#results').hide();
        $('#composition_methods_panel').hide();
        $('.composition-weight').hide();
        
        //ajax request for
        $(document).on('click', '#analysis-btn', function() {
            $('#results').hide();

            //validations
            $('#file-upload').popover('hide');
            $('#name').popover('hide');
            $('#email').popover('hide');
            $('#method').popover('hide');

            if ($('#file').val() == '') {
                $('#file-upload').popover('show');
                return;
            }

            if (!$('#name')[0].checkValidity()) {
                $('#name').popover('show');
                return;
            }

            if (!$('#email')[0].checkValidity()) {
                $('#email').popover('show');
                return;
            }

            if (!$('#method')[0].checkValidity()) {
                $('#method').popover('show');
                return;
            }

            $.ajax({
                url       : 'sentiment_analysis',
                type      : 'POST',
                data      : {'_token': $('#_token').val(),
                             'file': $('#file').val(),
                             'name': $('#name').val(),
                             'email': $('#email').val(),
                             'method': $('#method').val()
                            },
                async     : true,
                beforeSend: function() {
                    $('#loader').show();
                },
                success   : function(result){
                    console.log(result);
                    $('#result_console').html('<pre>'+result.result+'</pre>');
                    $('#result_download').html(result.download);
                    $('#loader').hide();
                    $('#results').show();
                },
                error     : function(error){
                    console.log(error);  
                },
                complete  : function() {
                    
                }
            });
        });

        $(document).on('change', '#method', function() {
            if ($('#method').val() == 'Composition') {
                $('#composition_methods_panel').show();
            } else {
                $('#composition_methods_panel').hide();
            }
        });

        $('#composition_methods').multiSelect({
            selectableHeader: '<div data-background-color="black" class="ms-header">Selectable methods:</div>',
            selectionHeader: '<div data-background-color="black" class="ms-header">Selected methods [weight]:</div>',
            keepOrder: true,
            afterSelect: function(values) {
                $( "ul.ms-list-selection" ).each(function( index ) {
                    $(this).unbind("click");
                });
            },
            afterDeselect: function(values) {
            }
        });

        /*$(document).on('click', '.method_weight', function(event) {
            event.preventDefault();
        });*/

        /*$(document).on('click', '.chkCompositionMethod', function() {
            if ($(this).is(':checked')) {
                $(this).parent().parent().find('.composition-weight').first().show();
            } else {
                $(this).parent().parent().find('.composition-weight').first().hide();
            }
        });*/
    });
</script>

</html>
