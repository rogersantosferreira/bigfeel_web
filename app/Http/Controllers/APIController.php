<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIController extends Controller
{
    public function upload(Request $request) {      //it was required to change upload_max_filesize and post_max_size in php.ini
        $default = ini_get('max_execution_time');
        set_time_limit(1200);                        //limits the excution time upper to 20 minutes (600 seconds)
        $path = $request->file('file')->store('');
        //shell_exec("hadoop fs -put $path /")      //puts the file in HDFS storage - only Linux
        set_time_limit($default);

        return $path;
    }

    public function afinn(Request $request, $id = null) {
        $method = 'Afinn';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

    	$default = ini_get('max_execution_time');
    	set_time_limit(600); 						//limits the excution time upper to 10 minutes (600 seconds)

    	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

    	$dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

    	set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

    	$res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function emolex(Request $request, $id = null) {
    	$method = 'Emolex';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function emoticons(Request $request, $id = null) {
        $method = 'Emoticons';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function emoticon_ds(Request $request, $id = null) {
        $method = 'EmoticonDS';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function happiness_index(Request $request, $id = null) {
        $method = 'HappinessIndex';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function mpqa(Request $request, $id = null) {
        $method = 'MPQA';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

	$dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function nrc(Request $request, $id = null) {
        $method = 'NRCHashtag';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function opinion(Request $request, $id = null) {
        $method = 'OpinionLexicon';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function panas_t(Request $request, $id = null) {
        $method = 'PanasT';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function sann(Request $request, $id = null) {
        $method = 'Sann';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function sasa(Request $request, $id = null) {
        $method = 'Sasa';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function sentic_net(Request $request, $id = null) {
        $method = 'SenticNet';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function sentiment140(Request $request, $id = null) {
        $method = 'Sentiment140';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function senti_strength(Request $request, $id = null) {
        $method = 'SentiStrength';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function socal(Request $request, $id = null) {
        $method = 'SoCal';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function stanford(Request $request, $id = null) {
        $method = 'Stanford';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function umigon(Request $request, $id = null) {
        $method = 'Umigon';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function vader(Request $request, $id = null) {
        $method = 'Vader';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);	

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function db_stanford(Request $request, $id = null) {
        $method = 'db_stanford';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function rf_spark(Request $request, $id = null) {
        $method = 'spark_random_forest';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function nb_spark(Request $request, $id = null) {
        $method = 'spark_naive_bayes';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function cleanxml(Request $request, $id = null) {
        $method = 'cleanxml';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function ssplit(Request $request, $id = null) {
        $method = 'ssplit';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

 	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);       

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function pos(Request $request, $id = null) {
        $method = 'pos';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function lemma(Request $request, $id = null) {
        $method = 'lemma';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function ner(Request $request, $id = null) {
        $method = 'ner';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function depparse(Request $request, $id = null) {
        $method = 'depparse';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function coref(Request $request, $id = null) {
        $method = 'coref';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function natlog(Request $request, $id = null) {
        $method = 'natlog';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function openie(Request $request, $id = null) {
        $method = 'openie';
        if (!$id) {
            $input = $request->file('file')->store('');
        } else {
            $input = $id;
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

        $output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method);

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }

    public function composition(Request $request, $id = null) {
        $method = 'Composition';
        if (!$id) {
            return 'Invalid input paramters!';
        } else {
            $params = explode("|", $id);
            $methods = $params[0];
            $weights = $params[1];
            if (count($params) == 2) {
                $input = $request->file('file')->store('');
            } else if (count($params == 3)){
                $input = $params[2];
            }
        }

        $default = ini_get('max_execution_time');
        set_time_limit(600);                        //limits the excution time upper to 10 minutes (600 seconds)

	$output = shell_exec("/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
                      env('SPARK_MEMORY', '2G')." --executor-memory ".
                      env('SPARK_MEMORY', '2G')." --master ".
                      env('SPARK_MASTER', 'local[*]')." ".env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
                      env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method." ".$methods." ".$weights);        

        $dir = utf8_encode('app/'.$input.'.'.$method.'.output/*.csv');

        set_time_limit($default);

        $result = '';
        $files = glob(storage_path($dir));
        foreach ($files as $file) {
            $result .= str_replace("\r\n", ',', file_get_contents($file));
            $result .= str_replace("\n", '', $result);
        }

        $output = str_replace("\n", '', $output);

        $res = ['output'   => $result,
                'log'      => $output];

        return $res;
    }
}
