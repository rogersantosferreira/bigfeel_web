<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SAController extends Controller
{
	// execute the sentiment analysis method defined by user
    public function analysis(Request $request) {

        //return ['result' => $request->all()];  //debug

    	$input 	= $request->file;
    	if (empty($input)) { return ['result' => 'Invalid input value!']; }
    	$email  = $request->email;
    	if (empty($email)) { return ['result' => 'Invalid email value!']; }
    	$name 	= $request->name;
    	if (empty($name)) { return ['result' => 'Invalid name value!']; }
    	$method = $request->method;
    	if (empty($method)) { return ['result' => 'Invalid method value!']; }

        $methods = '';
        $weights = '';
        $weights_r = [];
        $weight_sum = 0;

        if ($method == 'Composition') {
            $methods = $request->composition_methods;
            $weights = $request->composition_weights;
            if (empty($methods)) {
                return ['result' => 'Invalid method values for composition!'];
            }
            if (empty($weights)) {
                return ['result' => 'Invalid weight values for composition!'];
            }

            //validation of weight sum
            $weights_r = explode(',', $weights);
            $weight_sum = array_sum($weights_r);
            //dd($weight_sum);    //debug
            if ($weight_sum > 1.0) {
                return ['result' => 'Invalid weight values for composition ('.$weight_sum.')! The total sum is greater than 1.0!'];
            }
        }

    	$default = ini_get('max_execution_time');
    	set_time_limit(600); 						//limits the excution time upper to 10 minutes (600 seconds)

        if ($method != 'Composition') {
    	   $command = "/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
		     env('SPARK_MEMORY', '2G')." --executor-memory ".
		     env('SPARK_MEMORY', '2G')." --master ".env('SPARK_MASTER', 'local[*]')." ".
		     env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
		     env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method/*." 2>&1; echo $?"*/; //the commented part is for printing errors
	   //return ['result' => $command]; //DEBUG

	   //store log
	   $myfile = file_put_contents('logs.txt', date('Y-m-d H:i:s').' - '.$email.' '.$name.' '.$command.PHP_EOL , FILE_APPEND | LOCK_EX);

	   $output = shell_exec($command);
        } else {
           $command = "/cluster/spark/bin/spark-submit --class app.BigFeel --driver-memory ".
		     env('SPARK_MEMORY', '2G')." --executor-memory ".
		     env('SPARK_MEMORY', '2G')." --master ".
		     env('SPARK_MASTER', 'local[*]')." ".
		     env('BIGFEEL_JAR', 'C:\wamp\www\bigfeel\storage\app\bigfeel.jar')." ".
		     env('INPUT_ADDRESS', 'C:\wamp\www\bigfeel\storage\app\\').$input." ".$method." ".$methods." ".$weights/*." 2>&1; echo $?"*/; //the commented part is for printing errors 
	   //return ['result' => $command]; //DEBUG

	   //store log
	   $myfile = file_put_contents('logs.txt', date('Y-m-d H:i:s').' - '.$email.' '.$name.' '.$command.PHP_EOL , FILE_APPEND | LOCK_EX);

	   $output = shell_exec($command);
		//return ['result' => $output];
        }

    	$dir = utf8_encode('/download/'.$input.'.'.$method.'.output');

    	set_time_limit($default);					//resets max executin time

    	$res = ['result' 	=> $output,
    		'download' 	=> '<a href="'.$dir.'"><strong>Download the result</strong></a><br/><br/>
    							Please, contact us and give your opinion by email: rogersantosferreira@gmail.com'];

    	return $res;
    }

    //limited in 2MB file size
    public function upload(Request $request) {
    	$path = $request->file('file')->store('');
        return $path;
    }

    public function download($id) {
    	$files = glob(storage_path('app/'.$id.'/*'));
        \Zipper::make(public_path("$id.zip"))->add($files)->close();

        return response()->download(public_path("$id.zip"));
    }
}
